﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_xml_login
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				information info = new information();
				info.studentName = txtBoxName.Text;
				info.studentID = txtBoxID.Text;
				info.studentPassword = txtBoxPasword.Password;
				SaveXML.saveData(info, "bc12345678.xml");
				msgLabel.Visibility = Visibility.Visible;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnReset_Click(object sender, RoutedEventArgs e)
		{
			txtBoxName.Text = "";
			txtBoxID.Text = "";
			txtBoxPasword.Password = "";
			msgLabel.Visibility = Visibility.Hidden;
		}
	}
}
