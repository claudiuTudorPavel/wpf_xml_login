﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf_xml_login
{
	public class information
	{
		private string txtDataOne;
		private string txtDataTwo;
		private string txtDataThree;

		public string studentName
		{
			get { return txtDataOne; }
			set { txtDataOne = value; }
		}

		public string studentID
		{
			get { return txtDataTwo; }
			set { txtDataTwo = value; }
		}

		public string studentPassword
		{
			get { return txtDataThree; }
			set { txtDataThree = value; }
		}

	}
}
